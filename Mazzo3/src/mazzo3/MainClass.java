/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mazzo3;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author giovanni
 */
public class MainClass extends Application {

    @Override
    public void start(Stage primaryStage) {
        Text text = null;
        Node shape = null;
        Random random = new Random();
        Mazzo mazzo = new Mazzo();
        Collections.shuffle(mazzo.carte);
        boolean flag;
        int tmp, N = 0;

        //Finestra di alert
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Input");
        alert.setHeaderText("Decidi il numero di carte da mostrare");
        alert.setContentText("Consigliato: tra 10 e 30");
        alert.showAndWait();
        do {
            try {
                do {
                    //Finestra di input
                    System.out.println("Inserire un numero tra 0 e 104");
                    TextInputDialog dialog = new TextInputDialog("20");
                    dialog.setTitle("Numero carte");
                    dialog.setHeaderText("Inserisci un numero");
                    dialog.setContentText("Numero:");
                    String s = dialog.showAndWait().get();
                    tmp = Integer.parseInt(s);
                } while (tmp > 104 || tmp < 0); //controllo input
                N = tmp;
                flag = false;
            } catch (NumberFormatException ex) {
                System.out.println("Errore! Devi inserire un intero");
                flag = true;
            }
        } while (flag == true);

        //Metto in un array le prime N carte del mazzo
        Carta[] mano = new Carta[N];
        for (int i = 0; i < N; i++) {
            mano[i] = mazzo.carte.get(i);
        }
        System.out.println(Arrays.toString(mano)); //opzionale
        Carta winner = new Carta(-1, new Seme(-1));
        Carta loser = new Carta(-1, new Seme(-1));
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                //non efficiente, ma siccome parliamo al massimo di array da 104 elementi,
                //uso un metodo semplice e veloce da implementare
                if (mano[i].equals(mano[j]) && i != j) {
                    winner = mano[i];
                }
            }
        }
        if (winner.equals(loser)) {
            System.out.println("Hai perso");
        } else {

            System.out.println("Hai vinto!\nWinner: " + winner);
            System.out.println("Check doppia vittoria:");
            //prendi una carta a caso
            Carta acaso = mazzo.carte.get(random.nextInt(104));
            System.out.println("Carta pescata: " + acaso);
            if (acaso.numero == winner.numero) {
                System.out.println("Vittoria doppia!");
            } else {
                System.out.println("Niente vittoria doppia.");
            }

            //mostra la finestra
            winner.init();
            StackPane root = new StackPane(winner);
            root.setAlignment(Pos.CENTER);
            Scene scene = new Scene(root, 500, 500);
            scene.getStylesheets().add(MainClass.class.getResource("css.css").toExternalForm());
            primaryStage.setTitle("Vittoria!");
            primaryStage.setScene(scene);
            primaryStage.show();

            //vari test
            System.out.println("\nSort per numero: ");
            Collections.sort(mazzo.carte, new CartaPerValoreComparator());
            System.out.println(mazzo.carte);
            Collections.shuffle(mazzo.carte);
            System.out.println("Sort per seme: ");
            Collections.sort(mazzo.carte, new CartaPerSemeComparator());
            System.out.println(mazzo.carte);
            System.out.println("Test con HashSet");
            Set<Carta> test = new HashSet();
            test.addAll(mazzo.carte);
            System.out.println("Grandezza set: " + test.size());
        }

    }

    public static void main(String[] args) {
        launch(args);
    }
}
