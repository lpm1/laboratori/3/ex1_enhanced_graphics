/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mazzo3;

import java.util.Comparator;

/**
 *
 * @author giovanni
 */
public class CartaPerSemeComparator implements Comparator<Carta> {

    @Override
    public int compare(Carta o1, Carta o2) {
        if ((o1.seme.numero - o2.seme.numero) == 0) {
            if ((o1.numero - o2.numero) == 0) {
                return 0;
            } else if ((o1.numero - o2.numero) > 0) {
                return 1;
            } else {
                return -1;
            }
        } else if ((o1.seme.numero - o2.seme.numero) > 0) {
            return 1;
        } else {
            return -1;
        }
    }

}
