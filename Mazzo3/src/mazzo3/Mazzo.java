/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mazzo3;

import java.util.ArrayList;
import java.util.List;

public class Mazzo {

    List<Carta> carte;

    public Mazzo() {
        this.carte = new ArrayList(104);
        popolaMazzo();
    }

    private void popolaMazzo() {
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 13; i++) {
                for (int j = 0; j < 4; j++) {
                    this.carte.add(new Carta(i, new Seme(j)));
                }
            }
        }
    }

    @Override
    public String toString() {
        String res = "";
        for (int i = 0; i < 104; i++) {
            res += this.carte.get(i) + "\n";
        }
        return res;
    }
}
