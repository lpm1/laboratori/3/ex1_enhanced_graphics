/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mazzo3;

import javafx.scene.control.Button;

/**
 *
 * @author giovanni
 */
public class Seme extends Button {

    int numero;

    public Seme(int numero) {
        if (numero >= 0 && numero <= 3) {
            this.numero = numero;
        } else {
            this.numero = -1;
        }
        setPrefWidth(40);
        setPrefHeight(40);
        setRotate(180);
        getStyleClass().add(toString());
    }

    /*@Override
    Nel metodo init() di carta, viene chiamata GridPane.add(), che genera un
    IllegalArgumentException in quanto non si possono aggiungere duplicati,
    perciò questo metodo deve stare commentato.
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.numero;
        return hash;
    }*/
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Seme other = (Seme) obj;
        return this.numero == other.numero;
    }

    @Override
    public String toString() {
        String res = "";
        switch (this.numero) {
            case 0:
                res += "C";
                break;
            case 1:
                res += "Q";
                break;
            case 2:
                res += "F";
                break;
            case 3:
                res += "P";
                break;
        }
        return res;
    }
}
