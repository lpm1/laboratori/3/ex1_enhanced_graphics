/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mazzo3;

import java.util.Objects;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;

/**
 *
 * @author giovanni
 */
public class Carta extends GridPane {

    int numero;
    Seme seme;

    public Carta(int numero, Seme seme) {
        this.numero = numero + 1;
        this.seme = seme;
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int hash = prime * (this.numero + this.seme.numero + 35);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carta other = (Carta) obj;
        if (this.numero != other.numero) {
            return false;
        }
        return Objects.equals(this.seme, other.seme);
    }

    public String numeroToString() {
        String res = "";
        switch (this.numero) {
            case 1:
                res += "A";
                break;
            case 11:
                res += "J";
                break;
            case 12:
                res += "Q";
                break;
            case 13:
                res += "K";
                break;
            default:
                res += this.numero;
        }
        return res;
    }

    @Override
    public String toString() {
        String res = "";
        res += this.numeroToString();
        res += this.seme;
        return res;
    }

    public void init() {
        //dimensioni della carta
        setMaxHeight(450);
        setMinWidth(250);
        setMinHeight(450);
        setMaxWidth(250);
        getStyleClass().add("carta");

        Seme seme = null;
        /*Le carte 1 3 4 6 7 9 10 Q K vanno disegnate a partire dal centro,
          mentre le restanti vanno disegnate a partire dal lato, altrimenti
          verrebbero male.
         */
        for (int i = 0, j = (this.numero % 3 == 2 ? 0 : 1); i < this.numero; i++, j += 2) {
            seme = new Seme(this.seme.numero);
            if (this.numero == 1) {
                //is Ace()
                seme.setPrefHeight(200);
                seme.setPrefWidth(200);
            }
            add(seme, j % 3, (j / 3));
            setMargin(seme, new Insets(5));
        }

    }

}
